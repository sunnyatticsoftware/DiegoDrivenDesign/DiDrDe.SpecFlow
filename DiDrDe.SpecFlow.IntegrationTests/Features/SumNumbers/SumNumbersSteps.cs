﻿using DiDrDe.SpecFlow.WebApi;
using FluentAssertions;
using TechTalk.SpecFlow;

namespace DiDrDe.SpecFlow.IntegrationTests.Features.SumNumbers
{
    [Binding]
    public class SumNumbersSteps
    {
        private readonly Calculator _sut;
        private int _firstNumber;
        private int _secondNumber;
        private int _result;

        public SumNumbersSteps()
        {
            _sut = new Calculator();
        }

        [Given(@"I have entered (.*) into the calculator")]
        public void GivenIHaveEnteredIntoTheCalculator(int p0)
        {
            _firstNumber = p0;
        }

        [Given(@"I have also entered (.*) into the calculator")]
        public void GivenIHaveAlsoEnteredIntoTheCalculator(int p0)
        {
            _secondNumber = p0;
        }

        [When(@"I press add")]
        public void WhenIPressAdd()
        {
            _result = _sut.Sum(_firstNumber, _secondNumber);
        }

        [Then(@"the result should be (.*) on the screen")]
        public void ThenTheResultShouldBeOnTheScreen(int p0)
        {
            _result.Should().Be(p0);
        }
    }
}