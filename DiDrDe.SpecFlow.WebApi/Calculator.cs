﻿namespace DiDrDe.SpecFlow.WebApi
{
    public class Calculator
    {
        public int Sum(int firstNumber, int secondNumber)
        {
            var result = firstNumber + secondNumber;
            return result;
        }
    }
}